var h2 = 0;
casper.test.begin('pkshop.club is working', 2, function suite(test) {
    casper.start("https://pkshop.club", function() {
        test.assertHttpStatus(200); 
    });

    casper.then(function() {
        h2 = this.evaluate(function(){ 
            return document.querySelectorAll('h2').length;
        });
    });
    casper.then(function() {
        this.evaluate(function(){
            document.querySelector('input[name="shop"]').setAttribute('value', "Testing Shop");
            document.querySelector('form').submit();
        })
    })
    casper.then(function() {
        test.assertElementCount('h2', h2+1, "Shop added after submission");
    });
    casper.run(function() {
        test.done();
    });
});