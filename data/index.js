const Sequelize = require('sequelize');
const sequelize = new Sequelize('shops', 'username', 'password', {
  host: 'localhost',
  dialect: 'sqlite',
  // SQLite only
  storage: 'shops.sqlite',
  logging: true
});

const Shop = sequelize.define('shop', {
  shopName: {
    type: Sequelize.STRING
  },
  slug: {
    type: Sequelize.STRING
  },
  searchName: {
    type: Sequelize.STRING
  },
  averageRating: {
    type: Sequelize.REAL
  }
});

const Review = sequelize.define('review', {
  score: {
    type: Sequelize.INTEGER
  },
  text: {
    type: Sequelize.TEXT
  },
  helpful: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
});


Review.belongsTo(Shop);
Shop.hasMany(Review);
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    Shop.sync();
    Review.sync();
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

async function getRelatedShops() {
  let all_shops = await Shop.findAll();
  let r_index = parseInt(Math.ceil(Math.random() * all_shops.length), 10);
  let dummy = [];
  for (let i = r_index; i < r_index + 3; i++){
    dummy.push(all_shops[i % all_shops.length]);
  }
  all_shops = dummy;
  all_shops = all_shops.map(a => a.id);
  all_shops = `(${all_shops})`;
  const query = `select shops.id as id, count(reviews.id) as reviewcount, avg(reviews.score) as averageScore, shopName, text, slug, helpful, reviews.id as rid from shops left outer join reviews on reviews.shopId=shops.id where shops.id in ${all_shops} group by shops.shopName`;
  const options = {
    model: Shop,
    include: [{
      model: Review
    }]
  }
  let shops = await sequelize.query(query, options);
  shops = shops.map(shop => shop.dataValues);
  shops = shops.map(shop => {shop.averageScore = parseInt(Math.ceil(shop.averageScore),10); return shop});
  return new Promise((resolve, reject) => {
    resolve(shops);
  })
}
async function getHomePageShops() {
  const query = `select shops.id as id, count(reviews.id) as reviewcount, avg(reviews.score) as averageScore, shopName, text, slug, helpful, reviews.id as rid from shops left outer join reviews on reviews.shopId=shops.id group by shops.shopName having max(reviews.helpful) 
  union select shops.id as id, 0 as reviewcount, 0 as averageScore, shopName, text, slug, helpful, reviews.id as rid from shops left join reviews on reviews.shopId=shops.id where rid is NULL`;
  const options = {
    model: Shop,
    include: [{
      model: Review
    }]
  }
  let shops = await sequelize.query(query, options);
  shops = shops.map(shop => shop.dataValues);
  shops = shops.map(shop => {shop.averageScore = parseInt(Math.ceil(shop.averageScore),10); return shop});
  return new Promise((resolve, reject) => {
    resolve(shops);
  })
}
async function findShopBySlug(slug) {
  return Shop.findOne({
    where: { slug: slug },
    include: [{
        model: Review
    }]
  })
}
async function findShopById(id){
  return Shop.findById(id);
}
async function findReviewById(id){
  return Review.findById(id);
}

module.exports = { getHomePageShops, findShopBySlug, findReviewById, getRelatedShops, findShopById, Shop, Review };