var express = require('express');
var router = express.Router();
const db = require('../data/index');

function getReviewIdsFromSession(req){
  return req.cookies.review_ids;
}

function getVotesFromSession(req){
  return req.cookies.vote_array ? req.cookies.vote_array.split(",").map(a => parseInt(a, 10)) : [];
}
async function write_ratings(shop, score, text, req, res){
  let review = null;
  let found = false;
  review_ids_from_cookie = getReviewIdsFromSession(req);
  if (review_ids_from_cookie != null){
    review_ids_from_cookie = JSON.parse(review_ids_from_cookie);
    review_ids_from_cookie = review_ids_from_cookie.filter(r => r.shopId == shop.id);
    if (review_ids_from_cookie.length){
      found = true;
      review_ids_from_cookie = review_ids_from_cookie[0].id;
      review = await db.Review.findById(review_ids_from_cookie);
      await review.update({score: score === null ? review.score : score, text: text === null ? review.text : text});
    }
  }
  if (found === false){
    review = await db.Review.create({
      score,
      text,
      shopId: shop.id
    })
  }
  review_ids_from_cookie = getReviewIdsFromSession(req);
  if (review_ids_from_cookie == null){
    review_ids_from_cookie = [];
  }else{
    review_ids_from_cookie = JSON.parse(review_ids_from_cookie);
  }
  review_ids_from_cookie.push(review);
  res.cookie('review_ids', JSON.stringify(review_ids_from_cookie));1
  return review;
}

/* GET home page. */
router.get('/', async function(req, res, next) {
  const shops = await db.getHomePageShops();
  res.render('index', { shops });
});
async function getCommonElements(req, res){
  let shop = await db.findShopBySlug(req.params.slug);
  shop = shop.dataValues;
  shop.reviews = shop.reviews.map( a => a.dataValues )

  let posted_review = null;
  if (typeof(req.query.post_rating) !== "undefined"){
    posted_review = await write_ratings(shop, req.query.post_rating % 6, null, req, res);
  }else{
    if (getReviewIdsFromSession(req) != null){
      let review_ids_from_cookie = getReviewIdsFromSession(req);
      review_ids_from_cookie = JSON.parse(review_ids_from_cookie);
      review_ids_from_cookie = review_ids_from_cookie.filter(r => r.shopId == shop.id);
      if (review_ids_from_cookie.length){
        posted_review = await db.Review.findById(review_ids_from_cookie[0].id);
      }
    }
  }
  
  if (posted_review !== null){
    shop = await db.findShopBySlug(req.params.slug);
    shop = shop.dataValues;
    shop.reviews = shop.reviews.map( a => a.dataValues )
  }
  return { shop, posted_review };
}
router.get('/shop/:slug/:action/:id', async function(req, res, next) {
  let review = await db.findReviewById(req.params.id);
  if (req.params.action === "yes"){
    await review.update({
      helpful: review.helpful + 1
    })
  }else{
    await review.update({
      helpful: review.helpful - 1
    })
  }
  const shop = await db.findShopBySlug(req.params.slug);
  let review_votes = getVotesFromSession(req);
  review_votes.push(parseInt(req.params.id, 10));
  res.cookie('vote_array', "" + review_votes);
  let posted_review = null;
  if (getReviewIdsFromSession(req) != null){
    let review_ids_from_cookie = getReviewIdsFromSession(req);
    review_ids_from_cookie = JSON.parse(review_ids_from_cookie);
    review_ids_from_cookie = review_ids_from_cookie.filter(r => r.shopId == shop.id);
    if (review_ids_from_cookie.length){
      posted_review = await db.Review.findById(review_ids_from_cookie[0].id);
    }
  }
  const related_shops = await db.getRelatedShops();
  const { totalReviews, averageRating } = await getAverage(shop);
  res.render('shop', { shop, posted_review, path: `/shop/${req.params.slug}/`, 'vote_array': review_votes, related_shops, totalReviews, averageRating });
})
router.get('/shop/:slug', async function(req, res, next) {
  
  let { shop, posted_review } = await getCommonElements( req, res);
  let review_votes = getVotesFromSession(req);

  const related_shops = await db.getRelatedShops();
  
  const { totalReviews, averageRating } = await getAverage(shop);
  res.render('shop', { shop, posted_review, path: req.path ,'vote_array': review_votes, related_shops, totalReviews, averageRating});
});
router.post('/shop/:slug*', async function(req, res, next){
  let final_shop = await db.findShopBySlug(req.params.slug);
  final_shop = final_shop.dataValues;
  let review_text = req.body.review_text;
  await write_ratings(final_shop, null, review_text, req, res);
  let { shop, posted_review } = await getCommonElements( req, res);
  let review_votes = getVotesFromSession(req);
  const related_shops = await db.getRelatedShops();

  const { totalReviews, averageRating } = await getAverage(shop);
  res.render('shop', { shop, posted_review, 'vote_array': review_votes, path: `/shop/${req.params.slug}/`, related_shops, totalReviews, averageRating });
})
async function getAverage(shop){
  let totalReviews = shop.reviews.length;
  let averageRating = 0;
  shop.reviews.forEach(element => {
    averageRating += element.score;
  });
  averageRating = averageRating / totalReviews;
  averageRating = parseInt(Math.ceil(averageRating),10);
  return { averageRating, totalReviews};
}
function slugify(string) {
  return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}
router.post('/', async function(req, res, next) {
  await db.Shop.create({
    shopName: req.body.shop,
    slug: slugify(req.body.shop)
  })
  const shops = await db.getHomePageShops();
  res.render('index', { shops });
});
module.exports = router;
